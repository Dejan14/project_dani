<?php require_once("includes/connection.php"); ?>
<?php include_once("includes/functions.php"); ?>
<?php require_once("includes/layouts/header.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar.php"); ?>
<?php find_selected_dress(); ?>

    <section id="home"></section> <!-- End of home -->

<?php if ($current_dress) { ?>
<?php include_once("includes/request-form.php"); ?>

    <section id="dress"> <!-- Dress -->
        <div class="container">
            <div class="row">
                <div class="dress-photo col-sm-10 col-sm-offset-1 col-xs-12">
                    <div id="small-photo" class="small-photo col-sm-1">
                        <div class="photo-thumbnail col-sm-12 col-xs-3 current">
                            <img src="/img/dresses/<?php echo htmlentities($current_dress["dress_front"]); ?>" class="img-responsive">
                        </div>
                        <div class="photo-thumbnail col-sm-12 col-xs-3">
                            <img src="/img/dresses/<?php echo htmlentities($current_dress["dress_back"]); ?>" class="img-responsive">
                        </div>
                        <div class="photo-thumbnail col-sm-12 col-xs-3">
                            <img src="/img/dresses/<?php echo htmlentities($current_dress["dress_zoom"]); ?>" class="img-responsive">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="big-photo" class="big-photo col-sm-4">
                        <div class="current">
                            <img src="/img/dresses/<?php echo htmlentities($current_dress["dress_front"]); ?>" class="img-responsive">
                            <p data-toggle="modal" data-target="#modal-photo"><i class="fa fa-arrows-alt" aria-hidden="true"></i> View full screen.</p>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-photo">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="current text-center"><img src="/img/dresses/<?php echo htmlentities($current_dress["dress_front"]); ?>" class="img-responsive"></div>
                            </div>
                        </div>
                    </div>
                    <div class="dress-photo-right col-sm-7 col-xs-12">
                        <div class="dress-description">
                            <h2 class="dress-title">Haljina <?php echo htmlentities($current_dress["model"]); ?></h2>
                            <ul>
                                <li><b>Model: </b><?php echo htmlentities($current_dress["model"]); ?></li>
                                <li><b>Kolekcija: </b><?php echo htmlentities($current_dress["collection"]); ?></li>
                                <li><b>Materijal: </b><?php echo htmlentities($current_dress["fabric"]); ?></li>
                                <li><b>Dodaci: </b><?php echo htmlentities($current_dress["accessories"]); ?></li>
                                <li><b>Boja: </b><?php echo htmlentities($current_dress["color"]); ?></li>
                                <li><b>Veličina: </b><?php echo htmlentities($current_dress["size"]); ?></li>
                                <li><b>Opis: </b><?php echo htmlentities($current_dress["description"]); ?></li>
                            </ul>
                        </div>
                        <div class="dress-price">
                            <h2 class="dress-title">Upit za cenu modela</h2>
                            <form action="/haljine/<?php echo urlencode($current_dress["model"]); ?>" method="POST" class="form-horizontal">
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email:</label>
                                    <div class="col-md-8">
                                        <input type="email" id="email" class="form-control" name="email" aria-describedby="helpBlock" value="" placeholder="Vaš e-mail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-block" name="submit">POŠALJITE UPIT</button>
                                        <span id="helpBlock" class="help-block">Proverite da li ste dobro uneli Vašu e-mail adresu kako bismo mogli da Vam pošaljemo cenu modela.</span>
                                    </div>
                                </div>
                            </form>
                            <div class="form-message">

                            <?php
                                if(isset($text)) {
                                    echo "Nepostojeća e-mail adresa.";
                                } elseif(isset($mailSend)) {
                                    if(!$mailSend) {
                                        echo "Vaš upit je neuspešno poslat. Kontaktirajte nas na e-mail designbydani1@gmail.com.";
                                    } else {
                                        echo "Vaš upit je uspešno poslat.";
                                    }
                                }
                            ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="dress-photo col-sm-10 col-sm-offset-1 col-xs-12">
                    <div class="other-dress-photos col-sm-12">
                        <h2 class="dress-title">Haljine iz kolekcije <?php echo htmlentities($current_dress["collection"]); ?></h2>

                        <?php
                            $all_dresses_collection = find_dress_by_collection($current_dress["collection"]);
                            while($dresses_collection = mysqli_fetch_assoc($all_dresses_collection)) {
                        ?>

                        <div class="other-dress-photo col-sm-3 col-xs-6 text-center">
                            <a href="/haljine/<?php echo urlencode($dresses_collection["model"]); ?>" title="Haljina <?php echo htmlentities($dresses_collection["model"]); ?> | <?php echo htmlentities($dresses_collection["collection"]); ?>">
                                <img src="/img/dresses/<?php echo htmlentities($dresses_collection["dress_back"]); ?>" class="img-responsive photo-back" alt="<?php echo htmlentities($dresses_collection["model"]); ?>">
                                <img src="/img/dresses/<?php echo htmlentities($dresses_collection["dress_front"]); ?>" class="img-responsive photo-front">
                            </a>
                        </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section> <!-- End of Dress -->

<?php } else { ?>

    <section id="home-page"> <!-- All Collaborations -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 home-page-small">

                    <?php
                        $all_dresses = find_all_dresses();
                        while($dress = mysqli_fetch_assoc($all_dresses)) {
                    ?>

                    <div class="col-sm-3 col-xs-6 text-center">
                        <a href="/haljine/<?php echo urlencode($dress["model"]); ?>" title="Haljina <?php echo htmlentities($dress["model"]); ?> | <?php echo htmlentities($dress["collection"]); ?>">
                            <img src="/img/dresses/<?php echo htmlentities($dress["dress_back"]); ?>" class="img-responsive photo-back" alt="<?php echo htmlentities($dress["model"]); ?>">
                            <img src="/img/dresses/<?php echo htmlentities($dress["dress_front"]); ?>" class="img-responsive photo-front">
                        </a>
                    </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </section> <!-- End of All Collaborations -->

<?php } ?>

<?php include_once("includes/layouts/footer.php"); ?>
