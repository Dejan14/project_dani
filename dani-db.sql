-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: dani_blog
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `collaborations`
--

DROP TABLE IF EXISTS `collaborations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collaborations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `title_eng` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `text` mediumtext,
  `text_eng` mediumtext,
  `cover_photo` varchar(45) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collaborations`
--

LOCK TABLES `collaborations` WRITE;
/*!40000 ALTER TABLE `collaborations` DISABLE KEYS */;
INSERT INTO `collaborations` VALUES (1,'obucite-se-cice','get-dressed-girls','2015-09-17','Saradnja sa blogom \"Jedan frajer i bidermajer\".','Collaboration with blog \"Jedan frajer i bidermajer\".','biedermeier-00.jpg','http://jedanfrajeribidermajer.com/obucite-se-cice/'),(2,'story-party-2015','story-party-2015','2015-10-01','Roksanda Babić i Marija Veljković u našim unikatnim haljinama.','Roksanda Babic and Marija Veljkovic in our unique dresses.','story-00.jpg',NULL),(3,'dama','lady','2015-10-29','Saradnja sa blogom \"Niksis\".','Collaboration with blog \"Niksis\".','niksis4-00.jpg','http://niksis4.blogspot.rs/2015/10/lady.html'),(4,'zalazak-sunca','sunset','2015-11-13','\"Design by DANI\" unikatna haljina na blogu \"Niksis\".','\"Design by DANI\" unique dress on blog \"Niksis\".','sunset-00.jpg','http://niksis4.blogspot.rs/2015/11/sunset.html'),(5,'zlatna-jesen','golden-autumn','2015-11-17','Saradnja sa blogom \"Sugar Journal by Rendzi\".','Collaboration with blog \"Sugar Journal by Rendzi\".','golden-autumn-00.jpg','http://sugarjournalbyrendzi.blogspot.rs/2015/11/golden-autumn.html'),(6,'hello-awards-2015','hello-awards-2015','2015-12-10','Marija Veljković i Roksanda Babić u \"Design by DANI\" unikatnim haljinama.','Marija Veljkovic and Roksanda Babic in \"Design by DANI\" unique dresses.','hello-awards-2015-00.jpg',NULL),(7,'pevačica-kristina','singer-kristina','2016-02-05','Pevačica Kristina Kuzmanovska u \"Design by DANI\" haljini.','Singer Kristina Kuzmanovska in \"Design by DANI\" dress.','kristina-00.jpg','https://www.facebook.com/KristinaKuzmanovskaOFFICIAL/'),(8,'kristina-kuzmanovska','kristina-kuzmanovska','2016-02-12','Pevačica Kristina Kuzmanovska u haljini iz naše kolekcije \"Triangles\".','Singer Kristina Kuzmanovska in a dress from our collection \"Triangles\".','kristina02-00.jpg','https://www.facebook.com/KafanaSipajNePitaj/');
/*!40000 ALTER TABLE `collaborations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collaborations_photos`
--

DROP TABLE IF EXISTS `collaborations_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collaborations_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(45) DEFAULT NULL,
  `collaborations_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collaborations_photos`
--

LOCK TABLES `collaborations_photos` WRITE;
/*!40000 ALTER TABLE `collaborations_photos` DISABLE KEYS */;
INSERT INTO `collaborations_photos` VALUES (1,'biedermeier-01.jpg',1),(2,'biedermeier-02.jpg',1),(3,'biedermeier-03.jpg',1),(4,'biedermeier-04.jpg',1),(5,'biedermeier-05.jpg',1),(6,'story-01.jpg',2),(7,'story-02.jpg',2),(8,'niksis4-01.jpg',3),(9,'niksis4-02.jpg',3),(10,'niksis4-03.jpg',3),(11,'niksis4-04.jpg',3),(12,'niksis4-05.jpg',3),(13,'sunset-01.jpg',4),(14,'sunset-02.jpg',4),(15,'sunset-03.jpg',4),(16,'sunset-04.jpg',4),(17,'sunset-05.jpg',4),(18,'golden-autumn-01.jpg',5),(19,'golden-autumn-02.jpg',5),(20,'golden-autumn-03.jpg',5),(21,'hello-awards-2015-01.jpg',6),(22,'hello-awards-2015-02.jpg',6),(23,'kristina-01.jpg',7),(24,'kristina-02.jpg',7),(25,'kristina02-01.jpg',8),(26,'kristina02-02.jpg',8),(27,'kristina02-03.jpg',8);
/*!40000 ALTER TABLE `collaborations_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collections`
--

DROP TABLE IF EXISTS `collections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `cover_photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collections`
--

LOCK TABLES `collections` WRITE;
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;
INSERT INTO `collections` VALUES (1,'pastel-2015','dress-front-07.jpg'),(2,'triangles-2016','dress-front-06.jpg'),(3,'night-2016','dress-front-09.jpg');
/*!40000 ALTER TABLE `collections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collections_photos`
--

DROP TABLE IF EXISTS `collections_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collections_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(45) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `collections_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collections_photos`
--

LOCK TABLES `collections_photos` WRITE;
/*!40000 ALTER TABLE `collections_photos` DISABLE KEYS */;
INSERT INTO `collections_photos` VALUES (1,'01','dress-front-01.jpg',1),(2,'02','dress-front-02.jpg',1),(3,'03','dress-front-03.jpg',1),(4,'04','dress-front-04.jpg',2),(5,'05','dress-front-05.jpg',2),(6,'06','dress-front-06.jpg',2),(7,'07','dress-front-07.jpg',1),(8,'08','dress-front-08.jpg',3),(9,'09','dress-front-09.jpg',3),(10,'10','dress-front-10.jpg',3);
/*!40000 ALTER TABLE `collections_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dresses`
--

DROP TABLE IF EXISTS `dresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(45) DEFAULT NULL,
  `collection` varchar(45) DEFAULT NULL,
  `fabric` varchar(100) DEFAULT NULL,
  `fabric_eng` varchar(100) DEFAULT NULL,
  `accessories` varchar(100) DEFAULT NULL,
  `accessories_eng` varchar(100) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `color_eng` varchar(100) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `description` mediumtext,
  `description_eng` mediumtext,
  `dress_front` varchar(45) DEFAULT NULL,
  `dress_back` varchar(45) DEFAULT NULL,
  `dress_zoom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dresses`
--

LOCK TABLES `dresses` WRITE;
/*!40000 ALTER TABLE `dresses` DISABLE KEYS */;
INSERT INTO `dresses` VALUES (1,'01','Pastel 2015','Pamuk','Cotton','Biseri','Pearls','Svetlo Roze','Antique White','S','Unikatna haljina od pamučnog materijala sa ručno ušivenim biserima. Haljina nema elastina, ali savršeno pristaje figuri.','Cotton unique dress with hand-sewn pearls. The dress is not stretchable, but perfectly fits the figure.','dress-front-01.jpg','dress-back-01.jpg','dress-zoom-01.jpg'),(2,'02','Pastel 2015','Loran','Loran','Biseri','Pearls','Svetlo Roze','Old Lace','M','Unikatna haljina od rastegljivog materijala sa ručno ušivenim biserima.','Unique dress of stretch material with hand-sewn pearls.','dress-front-02.jpg','dress-back-02.jpg','dress-zoom-02.jpg'),(3,'03','Pastel 2015','Brokat','Brocade','Biseri','Pearls','Bela','White','M','Unikatna bela haljina od brokata sa ručno ušivanim biserima. Materijal je rastegljiv i izuzetno prijatan.','Unique white dress of brocade with hand-sewn pearls. The material is stretchable and very cozy.','dress-front-03.jpg','dress-back-03.jpg','dress-zoom-03.jpg'),(4,'04','Triangles 2016','Loran, Brokat','Loran, Brocade','Kristali','Crystals','Crna','Black','M','Unikatna crna haljina od rastegljivog materijala (loran) u kombinaciji sa brokatom i ručno ušivanim kristalima.','Unique black dress of stretchy material (loran) in combination with brocade and hand-sewn crystals.','dress-front-04.jpg','dress-back-04.jpg','dress-zoom-04.jpg'),(5,'05','Triangles 2016','Loran, Brokat','Loran, Brocade','Kristali','Crystals','Tamno Plava, Crna','Midnight Blue, Black','M','Tamno plava unikatna haljina od rastegljivog materijala (loran) u kombinaciji sa brokatom i kristalima.','Midnight blue unique dress of stretchy material (loran) in combination with brocade and crystals.','dress-front-05.jpg','dress-back-05.jpg','dress-zoom-05.jpg'),(6,'06','Triangles 2016','Loran, Brokat','Loran, Brocade','Kristali','Crystals','Bordo Crvena, Crna','Maroon, Black','L','Bordo crvena unikatna haljina. Materijal je loran u kombinaciji sa brokatom. Kristali su ušivani ručno.','Maroon unique dress. The material is loran combined with brocade. Crystals were sewn by hand.','dress-front-06.jpg','dress-back-06.jpg','dress-zoom-06.jpg'),(7,'07','Pastel 2015','Brokat','Brocade','Biseri','Pearls','Svetlo Roze','Misty Rose','M','Romantična i ženstvena unikatna haljina od brokata, sa diskretnim detaljima.','Romantic and feminine unique dress made of brocade, with discreet details.','dress-front-07.jpg','dress-back-07.jpg','dress-zoom-07.jpg'),(8,'08','Night 2016','Loran, Brokat','Loran, Brocade','Šljokice','Sequins','Crna','Black','M','Crna unikatna haljina od rastegljivog materijala (loran) u kombinaciji sa brokatom i šljokicama.','Black unique dress made of stretchy material (loran) in combination with brocade and sequin fabric.','dress-front-08.jpg','dress-back-08.jpg','dress-zoom-08.jpg'),(9,'09','Night 2016','Brokat','Brocade','Kristali','Crystals','Crna','Black','M','Unikatna mala crna haljina od brokatom u kombinaciji sa kristalima.\n','Unique little black dress of brocade in combination with crystals.','dress-front-09.jpg','dress-back-09.jpg','dress-zoom-09.jpg'),(10,'10','Night 2016','Brokat','Brocade','Kristali','Crystals','Crna, Kadet Plava','Black, Cadet Blue','M','Unikatna haljina sa ručno ušivenim kristalima. Materijal je brokat (rastegljiv).','Unique dress with hand-sewn crystals. The material is brocade (stretchable).\n','dress-front-10.jpg','dress-back-10.jpg','dress-zoom-10.jpg');
/*!40000 ALTER TABLE `dresses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-16 16:56:10
