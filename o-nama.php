<?php require_once("includes/layouts/header.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar.php"); ?>

    <section id="home"></section> <!-- End of home -->
    <section id="about">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-left">
                    <p>"Design by DANI" brend je namenjen modernim damama koje su u pokretu tokom celog dana, ali i za one koje žele da svojom posebnošću zablistaju na nekom događaju ili u večernjem izlasku.<br><br>
                        Iza mladog brenda "Design by DANI" stoji mlada modna dizajnerka Danijela Milić iz Beograda. Mlada umetnica kombinuje jednostavne krojeve sa prijatnim materijalima.<br><br>
                        Kreacije odišu čistim linijama, prelepim tkaninama i neizbežnim detaljima. Svaka haljina je ručni rad.</p>
                </div>
            </div>
        </div>
    </section> <!-- End of About -->

<?php include_once("includes/layouts/footer.php"); ?>
