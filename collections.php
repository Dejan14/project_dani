<?php require_once("includes/connection.php"); ?>
<?php include_once("includes/functions.php"); ?>
<?php require_once("includes/layouts/header-eng.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar-eng.php"); ?>
<?php find_selected_collection(); ?>

<?php if ($current_collection) { ?>

        <section id="home">
        <div class="container-fluid text-center">
            <h1><span><?php echo ucfirst(str_replace("-", " ", $current_collection["title"])); ?></span></h1>
        </div>
    </section> <!-- End of home -->
    <section id="home-page"> <!-- Collection -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div id="big-photo" class="col-sm-6 big-photo text-center">
                        <div class="current">
                            <img src="/img/dresses/<?php echo htmlentities($current_collection["cover_photo"]); ?>" class="img-responsive">
                        </div>
                    </div>

                    <?php
                        $collections_photos = find_photos_for_collection($current_collection["id"]);
                        while($collections_photo = mysqli_fetch_assoc($collections_photos)) {
                    ?>

                    <div id="small-photo" class="col-sm-3 col-xs-6 text-center">
                        <div class="photo-thumbnail current">
                            <img src="/img/dresses/<?php echo htmlentities($collections_photo["photo"]); ?>" class="img-responsive" title="Dress <?php echo htmlentities($collections_photo["model"]); ?> | <?php echo ucfirst(str_replace("-", " ", $current_collection["title"])); ?>">
                        </div>
                    </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </section> <!-- End of Collection -->

<?php } else { ?>

    <section id="home"></section> <!-- End of home -->
    <section id="home-page"> <!-- All Collections -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 home-page-small">

                    <?php
                        $all_collections = find_all_collections();
                        while($collection = mysqli_fetch_assoc($all_collections)) {
                    ?>

                    <div class="col-sm-4 text-center">
                        <a class="thumbnail" href="/collections/<?php echo urlencode($collection["title"]); ?>">
                            <img src="/img/dresses/<?php echo htmlentities($collection["cover_photo"]); ?>" class="img-responsive" alt="<?php echo htmlentities($collection["title"]); ?>">
                            <div class="caption">
                                <h1><?php echo ucfirst(str_replace("-", " ", $collection["title"])); ?></h1>
                            </div>
                        </a>
                    </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </section> <!-- End of All Collections -->

<?php } ?>

<?php include_once("includes/layouts/footer-eng.php"); ?>
