<?php
    if ($_SERVER['HTTP_HOST'] != 'www.dani.rs') {
        header('Location: http://www.dani.rs'.$_SERVER['REQUEST_URI']);
    }

    if(isset($_GET['page'])) {
        $page = $_GET['page'];
        $file = $page.".php";
        if(file_exists($file)) {
            include_once($file);
        } else {
            echo "Error 404. File not found.";
        }
    } else {
        $index = "home.php";
        if(file_exists($index)) {
            include_once($index);
        } else {
            die("Error 404. File not found.");
        }
    }
?>
