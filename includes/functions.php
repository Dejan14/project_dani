<?php

    function change_date_format($date) {
        $timestamp = strftime("%m/%d/%Y", strtotime($date));
        return $timestamp;
    }

    function confirm_query($result_set) {
		if (!$result_set) {
			die("Database query failed.");
		}
	}

    function find_all_dresses() {
		global $connection;

		$query  = "SELECT * ";
        $query .= "FROM dresses ";
        $query .= "ORDER BY id DESC";
        $result = mysqli_query($connection, $query);
        confirm_query($result);
		return $result;
	}

    function find_dress_by_collection($dress_collection) {
		global $connection;

		$safe_dress_collection = mysqli_real_escape_string($connection, $dress_collection);

		$query  = "SELECT * ";
		$query .= "FROM dresses ";
        $query .= "WHERE collection = '{$safe_dress_collection}' ";
        $query .= "ORDER BY id DESC ";
		$query .= "LIMIT 4";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
		return $result;
	}

	function find_dress_by_model($dress_model) {
		global $connection;

		$safe_dress_model = mysqli_real_escape_string($connection, $dress_model);

		$query  = "SELECT * ";
		$query .= "FROM dresses ";
        $query .= "WHERE model = '{$safe_dress_model}' ";
		$query .= "LIMIT 1";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
		if($subject_set = mysqli_fetch_assoc($result)) {
			return $subject_set;
		} else {
			return null;
		}
	}

    function find_selected_dress() {
        global $current_dress;

		if (isset($_GET["subject"])) {
			$current_dress = find_dress_by_model($_GET["subject"]);
		} else {
			$current_dress = null;
		}
	}

    function find_all_collections() {
		global $connection;

		$query  = "SELECT * ";
        $query .= "FROM collections ";
        $query .= "ORDER BY id DESC";
        $result = mysqli_query($connection, $query);
        confirm_query($result);
		return $result;
	}

    function find_collection_by_title($subject_title) {
		global $connection;

		$safe_subject_title = mysqli_real_escape_string($connection, $subject_title);

		$query  = "SELECT * ";
		$query .= "FROM collections ";
        $query .= "WHERE title = '{$safe_subject_title}' ";
		$query .= "LIMIT 1";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
		if($subject_set = mysqli_fetch_assoc($result)) {
			return $subject_set;
		} else {
			return null;
		}
	}

    function find_photos_for_collection($coll_id) {
		global $connection;

		$safe_coll_id = mysqli_real_escape_string($connection, $coll_id);

		$query  = "SELECT * ";
		$query .= "FROM collections_photos ";
		$query .= "WHERE collections_id = {$safe_coll_id} ";
		$query .= "ORDER BY id DESC";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
		return $result;
	}

    function find_selected_collection() {
        global $current_collection;

		if (isset($_GET["subject"])) {
			$current_collection = find_collection_by_title($_GET["subject"]);
		} else {
			$current_collection = null;
		}
	}

    function find_all_collaborations() {
		global $connection;

		$query  = "SELECT * ";
        $query .= "FROM collaborations ";
        $query .= "ORDER BY date DESC";
        $result = mysqli_query($connection, $query);
        confirm_query($result);
		return $result;
	}

    function find_collaboration_by_title($subject_title, $page_title) {
		global $connection;

		$safe_subject_title = mysqli_real_escape_string($connection, $subject_title);

		$query  = "SELECT * ";
		$query .= "FROM collaborations ";
		if ($page_title == "saradnje") {
            $query .= "WHERE title = '{$safe_subject_title}' ";
        } elseif ($page_title == "collaborations") {
            $query .= "WHERE title_eng = '{$safe_subject_title}' ";
        }
		$query .= "LIMIT 1";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
		if($subject_set = mysqli_fetch_assoc($result)) {
			return $subject_set;
		} else {
			return null;
		}
	}

    function find_collaboration_by_id($subject_id) {
		global $connection;

		$safe_subject_id = mysqli_real_escape_string($connection, $subject_id);

		$query  = "SELECT * ";
		$query .= "FROM collaborations ";
		$query .= "WHERE id = '{$safe_subject_id}' ";
		$query .= "LIMIT 1";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
        if($subject_set = mysqli_fetch_assoc($result)) {
			return $subject_set;
		} else {
			return null;
		}
	}

    function find_photos_for_collaboration($coll_id) {
		global $connection;

		$safe_coll_id = mysqli_real_escape_string($connection, $coll_id);

		$query  = "SELECT * ";
		$query .= "FROM collaborations_photos ";
		$query .= "WHERE collaborations_id = {$safe_coll_id} ";
		$query .= "ORDER BY id ASC";
		$result = mysqli_query($connection, $query);
		confirm_query($result);
		return $result;
	}

    function find_selected_collaboration() {
        global $current_collaboration;

		if (isset($_GET["subject"])) {
			$current_collaboration = find_collaboration_by_title($_GET["subject"], $_GET["page"]);
		} else {
			$current_collaboration = null;
		}
	}

?>
