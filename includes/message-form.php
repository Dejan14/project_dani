<?php
    if (isset($_POST['submit'])) {

        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        $validemail = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

        if(!empty($message) || !empty($email) || !$validEmail) {

            $to = "Dejan Milić <dekimilic14@gmail.com>";
            $subject = "Poruka je poslata sa sajta www.dani.rs";
            $headers[] = "From: {$name} <{$validemail}>";
            $headers[] = "Reply-To: {$validemail}";
            $headers[] = "Content-type: text/plain; charset=utf-8";
            $headers[] = "X-Mailer: PHP/" . phpversion();
            $headers = implode("\r\n", $headers);

            $mailSend = mail($to, $subject, $message, $headers);

        } else {
            $text = "Nepostojeća e-mail adresa.";
        }
    }
?>
