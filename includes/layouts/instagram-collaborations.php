                    <div class="blog-box">
                        <h3>Instagram</h3>
                        <div class="col-xs-6 instagram">
                            <a href="https://www.instagram.com/p/BDQYZrmq8h5/" target="_blank" title="Black unique dress with crystals">
                                <img src="/img/instagram/instagram-01.jpg" class="img-responsive" alt="instagram-01">
                            </a>
                        </div>
                        <div class="col-xs-6 instagram">
                            <a href="https://www.instagram.com/p/BDLSCRjq8gA/" target="_blank" title="Create your dream job">
                                <img src="/img/instagram/instagram-02.jpg" class="img-responsive" alt="instagram-02">
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-6 instagram">
                            <a href="https://www.instagram.com/p/BCqGfpXK8gL/" target="_blank" title="Pearl details">
                                <img src="/img/instagram/instagram-03.jpg" class="img-responsive" alt="instagram-03">
                            </a>
                        </div>
                        <div class="col-xs-6 instagram">
                            <a href="https://www.instagram.com/p/BCyKwVkK8kj/" target="_blank" title="Love my job">
                                <img src="/img/instagram/instagram-04.jpg" class="img-responsive" alt="instagram-04">
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-6 instagram">
                            <a href="https://www.instagram.com/p/BAzSFFaK8ri/" target="_blank" title="Design by DANI dress">
                                <img src="/img/instagram/instagram-05.jpg" class="img-responsive" alt="instagram-05">
                            </a>
                        </div>
                        <div class="col-xs-6 instagram">
                            <a href="https://www.instagram.com/p/BAVBfb6q8nC/" target="_blank" title="Love">
                                <img src="/img/instagram/instagram-06.jpg" class="img-responsive" alt="instagram-06">
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
