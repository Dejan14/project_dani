    <footer>
        <div class="container-fluid">
            <div class="row row-top">
                <div class="col-sm-12 text-center social">
                    <h5><b>Subscribe & Follow</b></h5>
                    <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/DANIfashion/" target="_blank" title="www.facebook.com/DANIfashion/">
                                <i class="fa fa-facebook fa-lg"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://instagram.com/design_by_dani/" target="_blank" title="www.instagram.com/design_by_dani/">
                                <i class="fa fa-instagram fa-lg"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.pinterest.com/daniimilic/" target="_blank" title="www.pinterest.com/daniimilic/">
                                <i class="fa fa-pinterest-p fa-lg"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-12 text-center small-nav">
                    <ul>
                        <li><a href="/">POČETNA</a></li>
                        <li>|</li>
                        <li><a href="/o-nama">O NAMA</a></li>
                        <li>|</li>
                        <li><a href="/haljine">HALJINE</a></li>
                        <li>|</li>
                        <li><a href="/kolekcije">KOLEKCIJE</a></li>
                        <li>|</li>
                        <li><a href="/saradnje">SARADNJE</a></li>
                        <li>|</li>
                        <li><a href="/kontakt">KONTAKT</a></li>
                    </ul>
                </div>
                <div class="col-xs-1 col-xs-offset-10 back-to-top">
                    <a href="" title="Scroll to top">
                        <i class="fa fa-hand-o-up fa-lg"></i>
                    </a>
                </div>
            </div>
            <hr>
            <hr style="border-top:2px solid #000">
            <div class="row">
                <div class="col-sm-12 text-center copyright">
                    <h5>Copyright <i class="fa fa-copyright"></i> 2016 Design by DANI</h5>
                </div>
            </div>
        </div>
    </footer>
        <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <!-- jQuery validation plugin -->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/js/messages_sr_lat.min.js"></script>
        <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <!-- Custom JavaScript -->
    <script type="text/javascript" src="/js/validate.js"></script>
    <script type="text/javascript" src="/js/custom.js"></script>
</body>
</html>
<?php
    if (isset($connection)) {
        mysqli_close($connection);
    }
?>
