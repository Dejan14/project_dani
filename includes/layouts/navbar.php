    <section id="navbar">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/img/dani-brend.png" class="img-responsive">
                    </a>
                </div> <!-- End Navbar Header -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php if(!isset($_GET['page']) || (isset($_GET['page']) && $_GET['page'] == "home")) { echo "class=\"active-page\"";} ?>>
                            <a href="/">POČETNA</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "o-nama") { echo "class=\"active-page\"";} ?>>
                            <a href="/o-nama">O NAMA</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "haljine") { echo "class=\"active-page\"";} ?>>
                            <a href="/haljine">HALJINE</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "kolekcije") { echo "class=\"active-page\"";} ?>>
                            <a href="/kolekcije">KOLEKCIJE</a>
                            <ul class="nav-dropdown">
                                <li><a href="/kolekcije/night-2016">Night 2016</a></li>
                                <li><a href="/kolekcije/triangles-2016">Triangles 2016</a></li>
                                <li><a href="/kolekcije/pastel-2015">Pastel 2015</a></li>
                            </ul>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "saradnje") { echo "class=\"active-page\"";} ?>>
                            <a href="/saradnje">SARADNJE</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "kontakt") { echo "class=\"active-page\"";} ?>>
                            <a href="/kontakt">KONTAKT</a>
                        </li>
                        <li>
                            <ul class="nav-social">
                                <li>
                                    <a href="https://www.facebook.com/DANIfashion/" target="_blank" title="www.facebook.com/DANIfashion/">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://instagram.com/design_by_dani/" target="_blank" title="www.instagram.com/design_by_dani/">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.pinterest.com/daniimilic/" target="_blank" title="www.pinterest.com/daniimilic/">
                                        <i class="fa fa-pinterest-p"></i>
                                    </a>
                                </li>
                                <li>
                                     |
                                </li>
                                <li>
                                    <a href="/en" title="English"><img src="/img/United-Kingdom-flag-icon.png" height="12"></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section> <!-- End Navbar -->
