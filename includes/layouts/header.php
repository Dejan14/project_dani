<html>
<head>
    <?php
        if(isset($_GET['page']) && $_GET['page'] == "haljine" && isset($_GET['subject'])) {
            $subject = $_GET['subject'];
            echo "<title>Haljina {$subject} | DANI</title>";

        } elseif(isset($_GET['subject'])) {
            $subject = $_GET['subject'];
            $edit_subject = ucwords(str_replace("-", " ", $subject));
            echo "<title>{$edit_subject} | DANI</title>";

        } elseif(isset($_GET['page']) && !isset($_GET['subject'])) {
            $page = $_GET['page'];
            $edit_page = ucfirst(str_replace("-", " ", $page));
            echo "<title>{$edit_page} | DANI</title>";

        } else {
            echo "<title>DANI | Unikatne haljine</title>";
        }
    ?>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Design by DANI unikatne haljine. Kreacije odišu čistim linijama, prelepim tkaninama i neizbežnim detaljima. Svaka haljina je ručni rad.">

    <?php
        if(isset($_GET['page']) && $_GET['page'] == "kolekcije" && isset($_GET['subject'])) {
            $subject = $_GET['subject'];

            switch ($subject) {
                case 'pastel-2015':
                    echo "<meta name=\"keywords\" content=\"pastel, pastel 2015, kolekcija pastel, dani pastel, design by dani pastel, dani kolekcija pastel\">";
                    break;
                case 'triangles-2016':
                    echo "<meta name=\"keywords\" content=\"triangles, triangles 2016, kolekcija triangles, dani triangles, design by dani triangles, dani kolekcija triangles\">";
                    break;
                case 'night-2016':
                    echo "<meta name=\"keywords\" content=\"night, night 2016, kolekcija night, dani night, design by dani night, dani kolekcija night\">";
                    break;
            }

        } elseif(isset($_GET['page']) && !isset($_GET['subject'])) {
            $page = $_GET['page'];

            switch ($page) {
                case 'home':
                    echo "<meta name=\"keywords\" content=\"dani, design by dani, haljine, unikatne haljine, svecane haljine, dani unikatne haljine, dani haljine, design by dani haljine\">";
                    break;
                case 'o-nama':
                    echo "<meta name=\"keywords\" content=\"danijela milic, danijela dani milic, dani milic, dani o nama, design by dani o nama, brend dani, brend design by dani\">";
                    break;
                case 'kolekcije':
                    echo "<meta name=\"keywords\" content=\"dani kolekcije, design by dani kolekcije, brend dani kolekcije, haljine kolekcije, unikatne haljine kolekcije, dani haljine kolekcije\">";
                    break;
                case 'saradnje':
                    echo "<meta name=\"keywords\" content=\"design by dani saradnje, kristina kuzmanovska dani, kristina kuzmanovska design by dani, marija veljkovic dani, marija veljkovic design by dani, roksanda babic dani, roksanda babic design by dani\">";
                    break;
                case 'kontakt':
                    echo "<meta name=\"keywords\" content=\"dani kontakt, design by dani kontakt, brend dani kontakt, dani haljine kontakt, design by dani haljine kontakt, unikatne haljine kontakt\">";
                    break;
            }

        } elseif(!isset($_GET['page']) && !isset($_GET['subject'])) {
            echo "<meta name=\"keywords\" content=\"dani, design by dani, haljine, unikatne haljine, svecane haljine, dani unikatne haljine, dani haljine, design by dani haljine\">";
        }
    ?>

    <meta name="author" content="Dejan Milić">
    <link rel="icon" href="/img/dani.png">
        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/css/style.css">
    <link type="text/css" rel="stylesheet" href="/css/responsive.css">
        <!-- Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-74208843-1', 'auto');
      ga('send', 'pageview');
    </script>
</head>
<body>
