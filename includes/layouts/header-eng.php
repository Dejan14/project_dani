<html>
<head>
    <?php
        if(isset($_GET['page']) && $_GET['page'] == "dresses" && isset($_GET['subject'])) {
            $subject = $_GET['subject'];
            echo "<title>Dress {$subject} | DANI</title>";

        } elseif(isset($_GET['subject'])) {
            $subject = $_GET['subject'];
            $edit_subject = ucwords(str_replace("-", " ", $subject));
            echo "<title>{$edit_subject} | DANI</title>";

        } elseif(isset($_GET['page']) && $_GET['page'] != "en" && !isset($_GET['subject'])) {
            $page = $_GET['page'];
            $edit_page = ucfirst(str_replace("-", " ", $page));
            echo "<title>{$edit_page} | DANI</title>";

        } else {
            echo "<title>DANI | Unique dresses</title>";
        }
    ?>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Design by DANI unique dresses. The creations reflect the clean lines, beautiful fabrics and inevitable details. Every dress is handmade.">

    <?php
        if(isset($_GET['page']) && $_GET['page'] == "en") {
            echo "<meta name=\"keywords\" content=\"dresses, unique dresses, dani dresses, dani unique dresses, design by dani dresses, design by dani unique dresses\">";
        }
    ?>

    <meta name="author" content="Dejan Milić">
    <link rel="icon" href="/img/dani.png">
        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/css/style.css">
    <link type="text/css" rel="stylesheet" href="/css/responsive.css">
        <!-- Google Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-74208843-1', 'auto');
      ga('send', 'pageview');
    </script>
</head>
<body>
