    <section id="navbar">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/en">
                        <img src="/img/dani-brend.png" class="img-responsive">
                    </a>
                </div> <!-- End Navbar Header -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "en") { echo "class=\"active-page\"";} ?>>
                            <a href="/en">HOME</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "about-us") { echo "class=\"active-page\"";} ?>>
                            <a href="/about-us">ABOUT US</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "dresses") { echo "class=\"active-page\"";} ?>>
                            <a href="/dresses">DRESSES</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "collections") { echo "class=\"active-page\"";} ?>>
                            <a href="/collections">COLLECTIONS</a>
                            <ul class="nav-dropdown">
                                <li><a href="/collections/night-2016">Night 2016</a></li>
                                <li><a href="/collections/triangles-2016">Triangles 2016</a></li>
                                <li><a href="/collections/pastel-2015">Pastel 2015</a></li>
                            </ul>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "collaborations") { echo "class=\"active-page\"";} ?>>
                            <a href="/collaborations">COLLABORATIONS</a>
                        </li>
                        <li <?php if(isset($_GET['page']) && $_GET['page'] == "contact") { echo "class=\"active-page\"";} ?>>
                            <a href="/contact">CONTACT</a>
                        </li>
                        <li>
                            <ul class="nav-social">
                                <li>
                                    <a href="https://www.facebook.com/DANIfashion/" target="_blank" title="www.facebook.com/DANIfashion/">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://instagram.com/design_by_dani/" target="_blank" title="www.instagram.com/design_by_dani/">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.pinterest.com/daniimilic/" target="_blank" title="www.pinterest.com/daniimilic/">
                                        <i class="fa fa-pinterest-p"></i>
                                    </a>
                                </li>
                                <li>
                                     |
                                </li>
                                <li>
                                    <a href="/" title="Serbian"><img src="/img/Serbia-Flag-icon.png" height="12"></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section> <!-- End Navbar -->
