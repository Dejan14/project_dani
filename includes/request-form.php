<?php
    if(isset($_POST['submit'])) {

        $email = $_POST['email'];
        $dressModel = $_GET["subject"];
        $validEmail = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

        if(!empty($email) || !$validEmail) {

            $to = "Dejan Milić <dekimilic14@gmail.com>";
            $subject = "Upit za cenu modela {$dressModel} sa sajta www.dani.rs";
            $message = "Koliko je Haljina {$dressModel}?";
            $headers[] = "From: <{$validEmail}>";
            $headers[] = "Reply-To: {$validEmail}";
            $headers[] = "Content-type: text/plain; charset=utf-8";
            $headers[] = "X-Mailer: PHP/" . phpversion();
            $headers = implode("\r\n", $headers);

            $mailSend = mail($to, $subject, $message, $headers);

        } else {
            $text = "Nepostojeća e-mail adresa.";
        }
    }
?>
