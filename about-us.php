<?php require_once("includes/layouts/header-eng.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar-eng.php"); ?>

    <section id="home"></section> <!-- End of home -->
    <section id="about">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-left">
                    <p>"Design by DANI" brand is designed for modern women who are on the move throughout the day, but also for those who want to apply their uniqueness shine on an event or an evening out.<br><br>
                        Behind the young brand "Design by DANI" is a young fashion designer Danijela Milic from Belgrade. The young artist combines simple cuts with pleasant materials.<br><br>
                        The creations reflect the clean lines, beautiful fabrics and inevitable details. Every dress is handmade.</p>
                </div>
            </div>
        </div>
    </section> <!-- End of About -->

<?php include_once("includes/layouts/footer-eng.php"); ?>
