/*global $, jQuery*/
(function ($) {
    "use strict"; // Start of use strict

    $(document).ready(function () {
        $('form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true
                }
            }
        });
    });
}(jQuery)); // End of use strict
