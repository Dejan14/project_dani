/*global $, jQuery*/
(function ($) {
    "use strict"; // Start of use strict

    // Preloader
    $(window).load(function () {
        // Preloader Home Page
        $('#status').fadeIn(1500).fadeOut(1500);
		$('#preloader').delay(2500).fadeOut(1500);
        // Preloader Other Pages
        $('#status-all').delay(100).fadeOut(1500);
		$('#preloader-all').delay(1000).fadeOut(1500);
	});

	$(window).scroll(function () {
        //Collapse the navbar on scroll
		if ($(".navbar").offset().top > 30) {
			$(".navbar-fixed-top").addClass("top-nav-collapse");
		} else {
			$(".navbar-fixed-top").removeClass("top-nav-collapse");
		}
        //Show scroll to top
        if ($("footer").offset().top > 1200) {
            $(".back-to-top a").css("display", "block");
        }
	});

    //Scroll to top
    $(document).ready(function () {
        $(".back-to-top a").click(function () {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false;
        });
    });

    //Dress photo galery
    $(document).ready(function () {
        $('#small-photo .photo-thumbnail').click(function () {
            $('#small-photo .photo-thumbnail').removeClass('current');
            $(this).addClass('current');
            var path = $(this).find('img').attr('src');
            $('#big-photo img').attr('src', path);
            $('#modal-photo img').attr('src', path);
        });
    });

    //Filmstrip items row
    $("document").ready(function () {
        var space = 0;
        $(".filmstrip-item").each(function (index, element) {
            $(element).css("left", space + "%");
            space += 25;
        });
    });

    //Filmstrip controller
    $("document").ready(function () {
        //Move left
        $("#arrowhead-left").click(function () {
            $("#filmstrip-main").animate({ left: "-=25%" }, 300);
            $("#arrowhead-right span").show();
            if ($("#filmstrip-main").position().left < ($('#filmstrip-main').width() * (-1.1))) {
                $("#arrowhead-left span").hide();
            }
        });
        //Move right
        $("#arrowhead-right").click(function () {
            $("#filmstrip-main").animate({ left: "+=25%" }, 300);
            $("#arrowhead-left span").show();
            if ($("#filmstrip-main").position().left > ($('#filmstrip-main').width() / (-3))) {
                $("#arrowhead-right span").hide();
            }
        });
    });

    //Hover photo of dress
    $(document).ready(function () {
        $("img.photo-front").hover(
            function () {
                $(this).stop().animate({"opacity": "0"}, 500);
            },
            function () {
                $(this).stop().animate({"opacity": "1"}, 500);
            }
        );
    });

}(jQuery)); // End of use strict
