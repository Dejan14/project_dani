<?php require_once("includes/connection.php"); ?>
<?php include_once("includes/functions.php"); ?>
<?php require_once("includes/layouts/header-eng.php"); ?>
<?php include_once("includes/layouts/facebook.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar-eng.php"); ?>
<?php find_selected_collaboration(); ?>

    <section id="home"></section> <!-- End of home -->

<?php if ($current_collaboration) { ?>

    <section id="blog"> <!-- Collaboration -->
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-9">
                    <div class="page-header">
                        <h1><?php echo ucwords(str_replace("-", " ", $current_collaboration["title_eng"]));?><br><small><?php echo change_date_format($current_collaboration["date"]);?></small></h1>
                    </div>
                    <hr>
                    <div class="blog-left">
                        <p><?php echo htmlentities($current_collaboration["text_eng"]);?></p>

                        <?php
                            $collaborations_photos = find_photos_for_collaboration($current_collaboration["id"]);
                            while($collaborations_photo = mysqli_fetch_assoc($collaborations_photos)) {
                        ?>

                        <img src="/img/collaborations/<?php echo htmlentities($collaborations_photo["photo"]); ?>" class="img-responsive" alt="<?php echo htmlentities($current_collaboration["title_eng"]); ?>">

                        <?php } ?>
                        <?php if(!empty($current_collaboration["link"])) { ?>

                        <p><span>Link: <a href="<?php echo htmlentities($current_collaboration["link"]); ?>" target="_blank"><?php echo htmlentities($current_collaboration["link"]); ?></a></span></p>

                        <?php } ?>

                    </div>
                    <div class="row next"> <!-- Previous and Next Collaboration -->
                        <div class="col-sm-3 col-sm-offset-3 col-xs-6 text-right"> <!-- Previous Collaboration -->

                        <?php
                            $id_previous = $current_collaboration["id"] - 1;
                            $previous_collaboration = find_collaboration_by_id($id_previous);
                            if ($previous_collaboration) {
                        ?>

                            <a class="thumbnail" href="<?php echo urlencode($previous_collaboration["title_eng"]); ?>" title="Previous">
                                <img src="/img/collaborations/<?php echo htmlentities($previous_collaboration["cover_photo"]); ?>" class="img-responsive" alt="<?php echo htmlentities($previous_collaboration["title_eng"]); ?>">
                                <div class="caption">
                                    <h3><span class="glyphicon glyphicon-hand-left"></span><br><?php echo ucwords(str_replace("-", " ", $previous_collaboration["title_eng"])); ?></h3>
                                </div>
                            </a>

                        <?php } ?>

                        </div>
                        <div class="col-sm-3 col-xs-6 text-left"> <!-- Next Collaboration -->

                        <?php
                            $id_next = $current_collaboration["id"] + 1;
                            $next_collaboration = find_collaboration_by_id($id_next);
                            if ($next_collaboration) {
                        ?>

                            <a class="thumbnail" href="<?php echo urlencode($next_collaboration["title_eng"]); ?>" title="Next">
                                <img src="/img/collaborations/<?php echo htmlentities($next_collaboration["cover_photo"]); ?>" class="img-responsive" alt="<?php echo htmlentities($next_collaboration["title_eng"]); ?>">
                                <div class="caption">
                                    <h3><span class="glyphicon glyphicon-hand-right"></span><br><?php echo ucwords(str_replace("-", " ", $next_collaboration["title_eng"])); ?></h3>
                                </div>
                            </a>

                        <?php } ?>

                        </div>
                    </div> <!-- End of Previous and Next Collaboration -->
                </div>
                <div class="col-sm-3 blog-right">
                    <div class="blog-box"> <!-- About us -->
                        <h3>About us</h3>
                        <p>"Design by DANI" brand is designed for modern women who are on the move throughout the day, but also for those who want to apply their uniqueness shine on an event or an evening out.</p>
                        <img src="/img/dani-01.jpg" class="img-responsive" alt="Design-by-DANI-01">
                        <p>Behind the young brand "Design by DANI" is a young fashion designer Danijela Milic from Belgrade. The young artist combines simple cuts with pleasant materials.</p>
                        <img src="/img/dani-02.jpg" class="img-responsive" alt="Design-by-DANI-02">
                        <p>The creations reflect the clean lines, beautiful fabrics and inevitable details. Every dress is handmade.</p>
                    </div>
                    <div class="blog-box"> <!-- Archives Collaboration -->
                        <h3>Archive</h3>

                        <?php
                            $prev_year = null;
                            $prev_month = null;
                            $date_collaboration = find_all_collaborations();
                            while($archives = mysqli_fetch_assoc($date_collaboration)) {
                                $new_year = strftime("%Y", strtotime($archives["date"]));
                                $new_month = strftime("%B", strtotime($archives["date"]));
                                if($prev_year != $new_year) {
                                    echo "<h4>". $new_year ."</h4>";
                                }
                                if($prev_month != $new_month) {
                                    echo "<h5><b>". $new_month ."</b></h5>";
                                }
                                    echo "<h5><i><a href=\"". urlencode($archives["title_eng"]) ."\">". ucwords(str_replace("-", " ", $archives["title_eng"])) ."</a></i></h5>";
                                $prev_year = $new_year;
                                $prev_month = $new_month;
                            }
                        ?>

                    </div>

                    <?php require_once("includes/layouts/instagram-collaborations.php"); ?> <!-- Instagram -->

                    <div class="facebook">
                        <h3>Facebook</h3>
                        <div class="fb-page" data-href="https://www.facebook.com/DANIfashion/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <div class="fb-xfbml-parse-ignore">
                                <blockquote cite="https://www.facebook.com/DANIfashion/">
                                    <a href="https://www.facebook.com/DANIfashion/">Design by DANI</a>
                                </blockquote>
                            </div>
                        </div>
                    </div> <!-- End of Facebook -->
                </div>
            </div>
        </div>
    </section> <!-- End of Collaboration -->

    <?php } else { ?>

    <section id="home-page"> <!-- All Collaborations -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 home-page-small">

                    <?php
                        $all_collaborations = find_all_collaborations();
                        while($collaboration = mysqli_fetch_assoc($all_collaborations)) {
                    ?>

                    <div class="col-sm-3 text-center">
                        <a class="thumbnail" href="/collaborations/<?php echo urlencode($collaboration["title_eng"]); ?>">
                            <img src="/img/collaborations/<?php echo htmlentities($collaboration["cover_photo"]); ?>" class="img-responsive" alt="<?php echo htmlentities($collaboration["title_eng"]); ?>">
                            <div class="caption caption-date">
                                <p><?php echo change_date_format($collaboration["date"]);?></p>
                            </div>
                            <div class="caption">
                                <h1><?php echo ucwords(str_replace("-", " ", $collaboration["title_eng"])); ?></h1>
                            </div>
                        </a>
                    </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </section> <!-- End of All Collaborations -->

<?php } ?>

<?php include_once("includes/layouts/footer-eng.php"); ?>
