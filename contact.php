<?php require_once("includes/message-form.php"); ?>
<?php require_once("includes/layouts/header-eng.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar-eng.php"); ?>

    <section id="home"></section> <!-- End of home -->
    <section id="contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <address>For all information, questions and suggestions</address>
                </div>
                <div class="col-sm-12 text-center">
                    <div class="col-sm-6">
                        <address>
                            <i class="fa fa-envelope-o"></i> Write to us<br>
                                designbydani1@gmail.com<br>
                        </address>
                    </div>
                    <div class="col-sm-6">
                        <address>
                            <i class="fa fa-phone"></i>  Call us<br>
                                +381 66 8089***<br>
                        </address>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="col-sm-12 text-center">
                        <address style="margin-bottom:10px">
                            <i class="fa fa-paper-plane-o"></i></span>  Send us a message
                        </address>

                        <?php if(isset($text)) { ?>
                                <span class="form-message">* Failed to send your message. Please try again or mail us to designbydani1@gmail.com.</span>
                        <?php } elseif(isset($mailSend)) {
                                if(!$mailSend) : ?>
                                    <span class="form-message">* Failed to send your message. Please mail us to designbydani1@gmail.com.</span>
                                <?php else : ?>
                                    <span class="form-message">Your message was sent successfully.</span>
                                <?php endif;
                            } ?>

                    </div>
                    <form action="/contact" method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="sr-only">>Name:</label>
                            <div class="col-sm-12 text-center">
                                <input type="text" id="name" class="form-control" name="name" placeholder="Your name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email:</label>
                            <div class="col-sm-12">
                                <input type="email" id="email" class="form-control" name="email" placeholder="Your e-mail">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="sr-only">Message:</label>
                            <div class="col-sm-12">
                                <textarea id="message" class="form-control" name="message" cols="20" rows="10" placeholder="Your message"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block" name="submit">SEND MESSAGE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> <!-- End of Contact -->

<?php include_once("includes/layouts/footer-eng.php"); ?>
