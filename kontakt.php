<?php require_once("includes/message-form.php"); ?>
<?php require_once("includes/layouts/header.php"); ?>
<?php include_once("includes/layouts/preloader-all.php"); ?>
<?php require_once("includes/layouts/navbar.php"); ?>

    <section id="home"></section> <!-- End of home -->
    <section id="contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <address>Za sve informacije, pitanja i sugestije</address>
                </div>
                <div class="col-sm-12 text-center">
                    <div class="col-sm-6">
                        <address>
                            <i class="fa fa-envelope-o"></i> Možete nam pisati<br>
                                designbydani1@gmail.com<br>
                        </address>
                    </div>
                    <div class="col-sm-6">
                        <address>
                            <i class="fa fa-phone"></i>  Pozvati nas<br>
                                +381 66 8089***<br>
                        </address>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="col-sm-12 text-center">
                        <address style="margin-bottom:10px">
                            <i class="fa fa-paper-plane-o"></i></span>  Poslati poruku
                        </address>

                        <?php if(isset($text)) { ?>
                                <span class="form-message">* Vaša poruka je neuspešno poslata. Pokušajte ponovo ili nas kontaktirajte na e-mail designbydani1@gmail.com.</span>
                        <?php } elseif(isset($mailSend)) {
                                if(!$mailSend) : ?>
                                    <span class="form-message">* Vaša poruka je neuspešno poslata. Kontaktirajte nas na e-mail designbydani1@gmail.com.</span>
                                <?php else : ?>
                                    <span class="form-message">Vaša poruka je uspešno poslata.</span>
                                <?php endif;
                            } ?>

                    </div>
                    <form action="/kontakt" method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="sr-only">Ime:</label>
                            <div class="col-sm-12 text-center">
                                <input type="text" id="name" class="form-control" name="name" placeholder="Vaše ime">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email:</label>
                            <div class="col-sm-12">
                                <input type="email" id="email" class="form-control" name="email" placeholder="Vaš e-mail">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="sr-only">Poruka:</label>
                            <div class="col-sm-12">
                                <textarea id="message" class="form-control" name="message" cols="20" rows="10" placeholder="Vaša poruka"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-block" name="submit">POŠALJITE PORUKU</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> <!-- End of Contact -->

<?php include_once("includes/layouts/footer.php"); ?>
