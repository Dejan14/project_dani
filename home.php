<?php require_once("includes/layouts/header.php"); ?>
<?php include_once("includes/layouts/facebook.php"); ?>
<?php include_once("includes/layouts/preloader.php"); ?>
<?php require_once("includes/layouts/navbar.php"); ?>

    <section id="home">
        <div class="jumbotron">
            <div id="carousel" class="carousel fade" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                    <li data-target="#carousel" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="/img/design-by-dani-01.jpg" alt="design-by-dani-01">
                    </div>
                    <div class="item">
                        <img src="/img/design-by-dani-02.jpg" alt="design-by-dani-02">
                    </div>
                    <div class="item">
                        <img src="/img/design-by-dani-03.jpg" alt="design-by-dani-03">
                    </div>
                    <div class="item">
                        <img src="/img/design-by-dani-04.jpg" alt="design-by-dani-04">
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- End of home -->
    <section id="home-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 home-page-small"> <!-- Dresses -->
                    <div class="page-header text-center">
                        <h1><a href="/haljine">UNIKATNE HALJINE</a></h1>
                    </div>
                    <div id="filmstrip"> <!-- Filmstrip -->
                        <div class="filmstrip-container">
                            <div id="filmstrip-main" class="filmstrip-main">
                                <div class="filmstrip-item">
                                    <a href="/haljine/10" title="Haljina 10 | Night 2016">
                                        <img src="/img/dresses/dress-back-10.jpg" class="img-responsive photo-back" alt="Haljina 10">
                                        <img src="/img/dresses/dress-front-10.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/09" title="Haljina 09 | Night 2016">
                                        <img src="/img/dresses/dress-back-09.jpg" class="img-responsive photo-back" alt="Haljina 09">
                                        <img src="/img/dresses/dress-front-09.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/08" title="Haljina 08 | Night 2016">
                                        <img src="/img/dresses/dress-back-08.jpg" class="img-responsive photo-back" alt="Haljina 08">
                                        <img src="/img/dresses/dress-front-08.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/07" title="Haljina 07 | Pastel 2015">
                                        <img src="/img/dresses/dress-back-07.jpg" class="img-responsive photo-back" alt="Haljina 07">
                                        <img src="/img/dresses/dress-front-07.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/06" title="Haljina 06 | Triangles 2016">
                                        <img src="/img/dresses/dress-back-06.jpg" class="img-responsive photo-back" alt="Haljina 06">
                                        <img src="/img/dresses/dress-front-06.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/05" title="Haljina 05 | Triangles 2016">
                                        <img src="/img/dresses/dress-back-05.jpg" class="img-responsive photo-back" alt="Haljina 05">
                                        <img src="/img/dresses/dress-front-05.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/04" title="Haljina 04 | Triangles 2016">
                                        <img src="/img/dresses/dress-back-04.jpg" class="img-responsive photo-back" alt="Haljina 04">
                                        <img src="/img/dresses/dress-front-04.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/03" title="Haljina 03 | Pastel 2015">
                                        <img src="/img/dresses/dress-back-03.jpg" class="img-responsive photo-back" alt="Haljina 03">
                                        <img src="/img/dresses/dress-front-03.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/02" title="Haljina 02 | Pastel 2015">
                                        <img src="/img/dresses/dress-back-02.jpg" class="img-responsive photo-back" alt="Haljina 02">
                                        <img src="/img/dresses/dress-front-02.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                                <div class="filmstrip-item">
                                    <a href="/haljine/01" title="Haljina 01 | Pastel 2015">
                                        <img src="/img/dresses/dress-back-01.jpg" class="img-responsive photo-back" alt="Haljina 01">
                                        <img src="/img/dresses/dress-front-01.jpg" class="img-responsive photo-front">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="arrowhead"> <!-- Filmstrip controller -->
                            <div id="arrowhead-left" class="col-xs-3"><span title="Left"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span></div>
                            <div id="arrowhead-right" class="col-xs-3 col-xs-offset-6 text-right"><span title="Right"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div> <!-- End of Filmstrip -->
                    <div id="home-page-small-screen"> <!-- Small screen for Filmstrip -->
                        <div class="col-sm-3 col-xs-6 text-center">
                            <a href="/haljine/10" title="Haljina 10 | Night 2016">
                                <img src="/img/dresses/dress-back-10.jpg" class="img-responsive photo-back" alt="Haljina 10">
                                <img src="/img/dresses/dress-front-10.jpg" class="img-responsive photo-front">
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6 text-center">
                            <a href="/haljine/09" title="Haljina 09 | Night 2016">
                                <img src="/img/dresses/dress-back-09.jpg" class="img-responsive photo-back" alt="Haljina 09">
                                <img src="/img/dresses/dress-front-09.jpg" class="img-responsive photo-front">
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6 text-center">
                            <a href="/haljine/08" title="Haljina 08 | Night 2016">
                                <img src="/img/dresses/dress-back-08.jpg" class="img-responsive photo-back" alt="Haljina 08">
                                <img src="/img/dresses/dress-front-08.jpg" class="img-responsive photo-front">
                            </a>
                        </div>
                        <div class="col-sm-3 col-xs-6 text-center">
                            <a href="/haljine/07" title="Haljina 07 | Pastel 2015">
                                <img src="/img/dresses/dress-back-07.jpg" class="img-responsive photo-back" alt="Haljina 07">
                                <img src="/img/dresses/dress-front-07.jpg" class="img-responsive photo-front">
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> <!-- End of Dresses -->
                <div class="col-sm-12 home-page-small"> <!-- Collaborations -->
                    <div class="page-header text-center">
                        <h1><a href="/saradnje">SARADNJE</a></h1>
                    </div>
                    <div class="col-sm-6 text-center">
                        <a class="thumbnail" href="/saradnje/obucite-se-cice">
                            <img src="/img/collaborations/home-biedermeier-05.jpg" class="img-responsive" alt="Obucite se, Cice">
                            <div class="caption">
                                <h1>Obucite se, Cice</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 text-center">
                        <a class="thumbnail" href="/saradnje/zalazak-sunca">
                            <img src="/img/collaborations/home-sunset-05.jpg" class="img-responsive" alt="Zalazak sunca">
                            <div class="caption">
                                <h1>Zalazak sunca</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 text-center">
                        <a class="thumbnail" href="/saradnje/kristina-kuzmanovska">
                            <img src="/img/collaborations/home-kristina02-03.jpg" class="img-responsive" alt="Kristina Kuzmanovska">
                            <div class="caption">
                                <h1>Kristina Kuzmanovska</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 text-center">
                        <a class="thumbnail" href="/saradnje/hello-awards-2015">
                            <img src="/img/collaborations/home-hello-awards-2015-01.jpg" class="img-responsive" alt="Hello! Awards 2015">
                            <div class="caption">
                                <h1>Hello! Awards 2015</h1>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 text-center">
                        <a class="thumbnail" href="/saradnje/story-party-2015">
                            <img src="/img/collaborations/home-story-01.jpg" class="img-responsive" alt="Story party 2015">
                            <div class="caption">
                                <h1>Story party 2015</h1>
                            </div>
                        </a>
                    </div>
                </div> <!-- End of Collaborations -->
                <div class="col-sm-12 home-page-small"> <!-- About us and Social -->
                    <div class="col-sm-4 text-center">
                        <div class="page-header">
                            <h2><a href="/o-nama">O NAMA</a></h2>
                        </div>
                        <div class="col-sm-12 social">
                            <a class="thumbnail" href="/o-nama">
                                <img src="/img/dani-02.jpg" class="img-responsive" alt="Design-by-DANI-04">
                                <div class="caption">
                                    <h1>Design by DANI</h1>
                                </div>
                            </a>
                        </div>
                    </div>

                    <?php include_once("includes/layouts/instagram-home.php"); ?>

                    <div class="col-sm-4 text-center">
                        <div class="page-header">
                            <h2><a href="https://www.facebook.com/DANIfashion/" target="_blank" title="facebook.com/DANIfashion/">FACEBOOK</a></h2>
                        </div>
                        <div class="col-sm-12 text-center facebook social">
                            <div class="fb-page" data-href="https://www.facebook.com/DANIfashion/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/DANIfashion/">
                                        <a href="https://www.facebook.com/DANIfashion/">Design by DANI</a>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End of About us and Social -->
            </div>
        </div>
    </section> <!-- End of Home page -->

<?php include_once("includes/layouts/footer-home.php"); ?>
